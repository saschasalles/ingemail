<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ymail</title>
    <script src="https://cdn.tiny.cloud/1/wh7hz3hluz87a95633a6y3u03r1c4gi0ph1q15vv3xiir521/tinymce/5/tinymce.min.js"></script>
    <script>
        tinymce.init({
            selector: 'textarea'
        });
    </script>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet">
</head>

<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-3 col-md-3 mail-status">
                <nav class="nav-links-align nav nav-pills flex-column">

                    <h1 class="nav-link title text-align-center">Ymail</h1>


                    <button class="nav-links-align btn btn-color btn-pos center-block" id="list-send-list" data-toggle="list" href="#list-send" aria-controls="send">
                        <img src="<?php echo e(asset('image/email.png')); ?>" alt="email picto" width="25" height="25">
                        Nouveau Courrier
                    </button>

                    <!-- <textarea>Next, use our Get Started docs to setup Tiny!</textarea> -->
                    <div class="nav-links-align">
                        <a class="<?php echo e((request()->is('reception')) ? 'active bg-info' : ''); ?> nav-link text-light d-flex justify-content-between align-items-center" id="mylist" role="tablist" href="<?php echo e(url('reception')); ?>">
                            Boite de Réception
                            <span class="badge badge-primary badge-pill">14</span>
                        </a>
                        <a class="<?php echo e((request()->is('brouillons')) ? 'active bg-info' : ''); ?> nav-link text-light d-flex justify-content-between align-items-center" href="<?php echo e(url('brouillons')); ?>">
                            Brouillons
                            <span class="badge badge-primary badge-pill">3</span>
                        </a>
                        <a class="<?php echo e((request()->is('envoyes')) ? 'active bg-info' : ''); ?> nav-link text-light d-flex justify-content-between align-items-center" href="<?php echo e(url('envoyes')); ?>">
                            Envoyés
                        </a>
                        <a class="<?php echo e((request()->is('archives')) ? 'active bg-info' : ''); ?> nav-link text-light d-flex justify-content-between align-items-center" href="<?php echo e(url('archives')); ?>">
                            Archives
                        </a>
                        <a class="<?php echo e((request()->is('indesirables')) ? 'active bg-info' : ''); ?> nav-link text-light d-flex justify-content-between align-items-center" href="<?php echo e(url('indesirables')); ?>">
                            Indésirables
                        </a>
                        <a class="<?php echo e((request()->is('corbeille')) ? 'active bg-info' : ''); ?> nav-link text-light d-flex justify-content-between align-items-center" href="<?php echo e(url('corbeille')); ?>">
                            Corbeille
                        </a>
                    </div>
                </nav>
                <hr>

                <h3 class="nav-link title text-align-center"><?php echo e(Auth::user()->name); ?></h3>

                <p class=" text-light align-items-center text-align-center">
                    Mon Compte
                </p>


                <div class="nav d-flex justify-content-around">
                    <a class="text-align-center nav-links-align " href="<?php echo e(route('logout')); ?>">
                        <img src="<?php echo e(asset('image/logout.png')); ?>" alt="login image" width="50" height="50">
                    </a>
                    <a class="text-align-center nav-links-align" href="#">
                        <img src="<?php echo e(asset('image/settings.png')); ?>" alt="login image" width="50" height="50">
                    </a>
                </div>
            </div>

            <div class="col-9 col-md-9 mail-recept">
                <div class="row align-items-center">
                    <h2 class="col-md-5 title-pos"><?php echo $__env->yieldContent('title'); ?></h2>
                    <input class="col-md-3 offset-md-3 form-control search-pos" type="text" placeholder="Rechercher..." aria-label="Search">
                </div>
                <hr>

                <div class="row ml-2">
                    <div class="col-4 col-md-4">
                        <div class="list-group" id="mylist" role="tablist">
                            <?php echo $__env->yieldContent('mails'); ?>



                            <div class="dropdown-menu dropdown-menu-sm" id="context-menu">
                                <a class="dropdown-item" href="#"><img src="<?php echo e(asset('image/trash.png')); ?>" alt="trash picto" width="25" height="25"> Supprimer </a>
                                <a class="dropdown-item" href="#"><img src="<?php echo e(asset('image/check.png')); ?>" alt="validate picto" width="25" height="25"> J'ai lu !</a>
                                <a class="dropdown-item" href="#"><img src="<?php echo e(asset('image/documents.png')); ?>" alt="archive picto" width="25" height="25"> Archiver</a>
                                <a class="dropdown-item" href="#"><img src="<?php echo e(asset('image/indesirable.png')); ?>" alt="indesirable picto" width="25" height="25"> Indésirable</a>
                            </div>
                        </div>
                    </div>



                    <div class="col-8 mail-content">
                        <div class="tab-content mt-3 ml-2" id="nav-tabContent">
                            <div class="tab-pane fade" id="list-tln" role="tabpanel" aria-labelledby="list-tln-list">
                                Je fais de super livraisons sur mon vélo...
                            </div>
                            <div class="tab-pane fade" id="list-alex" role="tabpanel" aria-labelledby="list-alex-list">
                                Il y a deux types d'hommes dans la vie...
                            </div>
                        </div>



                        <div class="tab-pane fade envoi ml-2 mb-2" id="list-send" role="tabpanel" aria-labelledby="list-send-list">
                            <form method="POST">
                                <div class="form-group">
                                    <label for="exampleFormControlInput1">Destinataire (mail)</label>
                                    <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="uneadresse@example.com">
                                </div>
                                <div class="form-group">
                                    <label for="exampleFormControlInput1">Objet</label>
                                    <input type="string" class="form-control" id="exampleFormControlInput1" placeholder="Mon objet">
                                </div>

                                <div class="form-group">
                                    <textarea style="border-radius: 10%;">Tapez votre message ici!</textarea>
                                </div>
                            </form>
                            <button class="btn btn-color" type="submit">Envoyer</button>
                            <button class="btn btn-info" type="submit">Brouillon</button>
                            <button class="btn btn-outline-warning" type="submit">Abandonner</button>
                        </div>
                        <div class="container">
                            <?php echo $__env->yieldContent('content'); ?>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
    </div>




    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script>
        $('.jumbotron').on('contextmenu', function(e) {
            var top = e.pageY - 130;
            var left = e.pageX - 440;
            $("#context-menu").css({
                display: "block",
                top: top,
                left: left
            }).addClass("show");
            return false;
        }).on("click", function() {
            $("#context-menu").removeClass("show").hide();
        });

        $("#context-menu a").on("click", function() {
            $(this).parent().removeClass("show").hide();
        });


        $("html").on("click", function() {
            $("#context-menu").removeClass("show").hide();
        });


        $("html").mouseup(function(e) {
            var container = $("#tablist");
            if (!container.is(e.target) && container.has(e.target).length === 0) {
                $('.active').toggleClass('active');
            }
        });
    </script>
</body>

</html><?php /**PATH /Users/sascha/Projets/2019/ingemail/laravel/resources/views/layout.blade.php ENDPATH**/ ?>