<?php $__env->startSection('title'); ?>
Réception
<?php $__env->stopSection(); ?>

<?php $__env->startSection('mails'); ?>
    <a class="jumbotron list-group-item list-group-item-action align-items-start" id="list-tln-list" data-toggle="list" href="#list-tln" role="tab" aria-controls="tln">
        <div class="d-flex w-100 justify-content-between">
            <h5 class="mb-1">Test</h5>
            <small>Le 20/10/19 </small>
        </div>
        <p class="mb-1">Objet : test</p>
        <small>test</small>
    </a>

    <a class="jumbotron list-group-item list-group-item-action align-items-start" id="list-alex-list" data-toggle="list" href="#list-alex" role="tab" aria-controls="alex">
        <div class="d-flex w-100 justify-content-between">
            <h5 class="mb-1">Alex Boisseau</h5>
            <small>Il y a 2 jours</small>
        </div>
        <p class="mb-1">Objet : Parainnage Kuvéra</p>
        <small>Il y a deux types d'hommes dans la vie...</small>
    </a>


   
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Users/sascha/Projets/2019/ingemail/laravel/resources/views/reception.blade.php ENDPATH**/ ?>