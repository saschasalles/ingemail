# Installation et exécution en local:  
* Pré-requis, avoir git, Laravel 6 (avec composer installé), ainsi que node, et une pile LAMP/MAMP/WAMP..  
* git clone git@gitlab.com:Sascha_40/ingemail.git  
* Se placer dans le dossier Ymail : `cd Ymail/` 
* Editer un nouveau .env à partir du .env.example  
* Générer une nouvelle clef avec `php artisan  key:generate`  
* Connecter la base de données dans le .env  
* Migrer les données dans la base: `php artisan migrate`  
* Faire un `composer install`  
* Faire un `npm install`  
* Lancer le serveur laravel avec `php artisan serve` , aller sur `127.0.0.1:8000`  
