@extends('profile')


@section('title')
    <div class="row">
        <h2 class="col-md-9 title-pos d-none d-md-block">Réception</h2>
        <h5 class="col-md-9 title-pos d-xs-block d-md-none">Réception</h5>
    </div>
    <hr>


    @section('content')

    @if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
    @endif


    <div class="row">
        <div class="col-4 col-md-4 d-none d-md-block">
            <div class="list-group" role="tablist">
            @foreach($mails as $mail)
                <a class="jumbotron list-group-item list-group-item-action align-items-start" id="list-{{ $mail->id }}-list" data-toggle="list" href="#list-{{ $mail->id }}" role="tab" aria-controls="{{ $mail->id }}">
                    <div class="d-flex w-100 justify-content-between">
                        <h6 class="mb-1 text-dark">{{ $mail->name }}</h6>
                        <small>{{ date('l', strtotime($mail->created_at)) }}</small>
                    </div>
                    <p class="mb-1 objectfont">{{ $mail->object }}</p>
                    <small class="d-none d-lg-block contentfont ">{{Str::limit($mail->content , 40, ' ...')}}</small>
                </a>
                @endforeach
                <div class="dropdown-menu dropdown-menu-sm" id="context-menu">

                    <form action="{{ route('mails.destroy', $mail->id)}}" method="post">
                        @csrf
                        @method('DELETE')
                        <button class="dropdown-item" type="submit"><img src="{{ asset('images/trash.png') }}" alt="trash picto" width="25" height="25"> Supprimer</button>
                    </form>
                    <a class="dropdown-item" href="#"><img src="{{ asset('images/check.png') }}" alt="validate picto" width="25" height="25"> J'ai lu !</a>
                    <a class="dropdown-item" href="#"><img src="{{ asset('images/documents.png') }}" alt="archive picto" width="25" height="25"> Archiver</a>
                    <a class="dropdown-item" href="#"><img src="{{ asset('images/indesirable.png') }}" alt="indesirable picto" width="25" height="25"> Indésirable</a>
                </div>
            </div>
        </div>

        <div class="list-group pl-3  mx-auto col-11 d-xs-block d-md-none">
            @foreach($mails as $mail)
                <a href="{{ route('mails.show', $mail->id) }}" class="list-group-item list-group-item-action align-items-start py-1">
                    <div class="d-flex w-100 justify-content-between">
                        <h6 class="mb-1 text-dark">{{ $mail->name }}</h6>
                        <small>{{ date('D', strtotime($mail->created_at)) }}</small>
                    </div>
                    <p class="mb-1 text-muted">{{ $mail->object }}</p>
                    <small>{{Str::limit($mail->content , 35, ' ...')}}</small>
                </a>
            @endforeach
        </div>

            <div class="col-8 mail-content d-none d-md-block">
                <div class="tab-content mt-3 ml-2" id="nav-tabContent">
                    @foreach($mails as $mail)
                    <div class="tab-pane fade" id="list-{{ $mail->id }}" role="tabpanel" aria-labelledby="list-{{ $mail->id }}-list">
                        {{ $mail->content }}
                    </div>
                    @endforeach
                </div>
            </div>


    </div>

@endsection
@endsection
