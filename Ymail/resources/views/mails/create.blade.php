@extends('profile')

@section('title')
<div class="row ">
    <h2 class="col-md-9 title-pos d-none d-md-block">Nouveau Courriel</h2>
    <h5 class="col-md-9 title-pos d-xs-block d-md-none">Nouveau Courriel</h5>
</div>
<hr>
@endsection

@section('content')



    <div class="mb-2 col-lg-8 col-md-11 col-sm-9">
        <form action="{{ route('mails.store') }}" method="POST">
            @csrf
            @method('POST')
            <div class="form-group">
                <label for="exampleFormControlInput1">Destinataire (mail)</label>
                <input name="user" type="mail" class="form-control col-md-7" id="exampleFormControlInput1" placeholder="uneadresse@example.com">
            </div>
            <div class="form-group">
                <label for="exampleFormControlInput1">Expediteur</label>
                <input name="name" type="text" class="form-control col-md-7" id="exampleFormControlInput1" placeholder="Nom">
            </div>
            <div class="form-group">
                <label for="exampleFormControlInput1">Objet</label>
                <input name="object" type="text" class="form-control col-md-7" id="exampleFormControlInput1" placeholder="Mon objet">
            </div>

            <div class="form-group">
                <textarea name="content" style="border-radius: 10%;">Tapez votre message ici!</textarea>
            </div>

            <button class="btn btn-color btn-sm" type="submit">Envoyer</button>
            <button class="btn btn-info btn-sm" type="submit">Brouillon</button>
            <button class="btn btn-outline-warning btn-sm" type="submit">Abandonner</button>
        </form>
    </div>

@endsection
