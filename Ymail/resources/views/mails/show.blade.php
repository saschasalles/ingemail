@extends('profile')

@section('content')

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif


    <div class="row">
        <div class="col-12 col-md-12 mail-recept mx-auto d-xs-block d-md-none py-1">
            <div class="row py-0">
                <h5 class="col-md-9 title-pos d-xs-block d-md-none">{{$mail->object}}</h5>
                <hr>
                <p class="col-md-9  d-xs-block d-md-none py-1"><span>De : </span>{{ $mail->name }}</p>
                <p class="col-md-9 d-xs-block d-md-none py-1 d-xs-block d-md-none py-0" ><span>Le : </span>{{ date('l', strtotime($mail->created_at)) }}</p>
                <form action="{{ route('mails.destroy', $mail->id)}}" method="post">
                    @csrf
                    @method('DELETE')
                    <button class=" ml-3 d-xs-block d-md-none py-1 btn btn-danger btn-sm" type="submit">Supprimer</button>
                </form>
                <button class=" ml-1 d-xs-block d-md-none py-1 btn btn-info btn-sm" type="submit">Archiver</button>
                <button class="ml-1 d-xs-block d-md-none py-1 btn btn-outline-warning btn-sm" type="submit">Indésirable</button>
            </div>

            <div class=" mt-2 mb-2 content-mobile mx-auto col-sm-9 d-xs-block d-md-none py-1">
                {{$mail->content}}
            </div>
        </div>





    </div>

@endsection

