@extends('profile')


@section('title')
Indésirables
@endsection

@section('mails')
        <a class="jumbotron list-group-item list-group-item-action align-items-start" id="list-tln-list" data-toggle="list" href="#list-tln" role="tab" aria-controls="tln">
            <div class="d-flex w-100 justify-content-between">
                <h5 class="mb-1">Thomas le Naour</h5>
                <small>Il y a 3 jours</small>
            </div>
            <p class="mb-1">Objet : Asy on se capte les gars !</p>
            <small>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean pretium ullamcorper...</small>
        </a>

        <a class="jumbotron list-group-item list-group-item-action align-items-start" id="list-alex-list" data-toggle="list" href="#list-alex" role="tab" aria-controls="alex">
                <div class="d-flex w-100 justify-content-between">
                    <h5 class="mb-1">Alex Boisseau</h5>
                    <small>Il y a 2 jours</small>
                </div>
                <p class="mb-1">Objet : Parainnage Kuvéra</p>
                <small>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean pretium ullamcorper...</small>
        </a>
    
        <a class="jumbotron list-group-item list-group-item-action align-items-start" id="list-antoine-list" data-toggle="list" href="#list-antoine" role="tab" aria-controls="antoine">
                <div class="d-flex w-100 justify-content-between">
                    <h5 class="mb-1">Antoine Delbrel</h5>
                    <small>Hier</small>
                </div>
                <p class="mb-1">Objet : Absence Cours de Sécurité</p>
                <small>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean pretium ullamcorper...</small>
        </a> 
        
        <a class="jumbotron list-group-item list-group-item-action align-items-start" id="list-antoine-list" data-toggle="list" href="#list-antoine" role="tab" aria-controls="antoine">
                <div class="d-flex w-100 justify-content-between">
                    <h5 class="mb-1">Antoine Delbrel</h5>
                    <small>Hier</small>
                </div>
                <p class="mb-1">Objet : Absence Cours de Sécurité</p>
                <small>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean pretium ullamcorper...</small>
        </a>  
@endsection
