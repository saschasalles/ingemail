<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>Ymail</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <style>
        html,
        body {
            background-color: #23b2a4;
            color: #fff;
            font-family: "Montserrat", "Helvetica", "Arial", sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }


        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links>a {
            color: #fff;
            padding: 0 25px;
            text-decoration: none;
            font-weight: 400;
        }
        .links>a:hover{
            color: #e94e6d;
        }

        .m-b-md {
            margin-bottom: 30px;
        }

    </style>
    
</head>

<body>
    <div class="flex-center position-ref full-height">
       
        <div class="content">
            <div class="title m-b-md">
                Ymail
            </div>
                 @if (Route::has('login'))
                <div class="links m-b-md">
                    @auth
                    <a href="{{ url('/home') }}">Home</a>
                    @else
                    <a href="{{ route('login') }}">Connexion</a>

                    @if (Route::has('register'))
                    <a href="{{ route('register') }}">Inscription</a>
                    @endif
                    @endauth
                </div>
                @endif
    </div>
    
</body>

</html>
