<?php

namespace App\Http\Controllers;

use App\Mail;
use Illuminate\Http\Request;

class MailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mails = Mail::all();

        return view('mails.index', compact('mails'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('mails.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'user' => 'required',
            'object' => 'required',
            'content' => 'required',
        ]);
;
        // $mail = new Mail([
        //     'user' => $request->get('user'),
        //     'object' => $request->get('object'),
        //     'content' => $request->get('content'),
        // ]);

        Mail::create([
                'name' => $request->name,
                'user' => $request->user,
                'object' => $request->object,
                'content' => $request->content  ,
            ]);
        // $mail->save();

        return redirect()->route('mails.index')
                        ->with('success', 'Mail envoyé');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $mail= Mail::find($id);
        return view('mails.show', compact('mail'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Mail  $mail
     * @return \Illuminate\Http\Response
     */
    public function edit(Mail $mail)
    {
        return view('mails.edit', compact('mail'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mail  $mail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Mail $mail)
    {
        $request->validate([
            'user' => 'required',
            'object' => 'required',
            'content' => 'required',
        ]);

        $mail->update($request->all());

        return redirect()->route('mails.index')
                        ->with('success', 'Mail update successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mail = Mail::find($id);
        $mail->delete();

        return redirect('/mails')->with('success', 'Mail supprimé !');
    }
}
