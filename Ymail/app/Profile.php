<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = [
        'id', 'name', 'mail_address', 'password', 'nb_contact', 'nb_mails'
    ];

    public function users(){
        return $this->belongsTo('App\User');
    }

    public function mails(){
        return $this->hasMany('App\Mail');
    }
}
