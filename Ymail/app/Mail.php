<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mail extends Model
{
    protected $fillable = [
        'id','name', 'date', 'content', 'object', 'user', 'state'
    ];

    public function profiles(){
        return $this->belongsTo('App\Profile');
    }
    public function users(){
        return $this->belongsTo('App\User');
    }
}

