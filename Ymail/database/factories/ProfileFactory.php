<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Illuminate\Database\Seeder;
use App\Profile;
use Faker\Generator as Faker;

$factory->define(Profile::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'mail_address' => $faker->mail_address,
        'password' => $faker->password,
        'nb_contact' => $faker->nb_contact,
        'nb_mails' => $faker->nb_mails, 
    ];
});
