<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Mail;
use Faker\Generator as Faker;

$factory->define(Mail::class, function (Faker $faker) {
    return [
        'user' => $faker->user,
        'date' => $faker->date,
        'object' => $faker->object,
        'content' => $faker->content,
    ];
});
