<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * 
     */
    public function run()
    {
        factory(App\User::class, 10)->create()->each(function ($user){
            $user->assignRole('profile');
            // $user->mails()->save(factory(App\Mail::class)->make());
        });
        
        
    }
}
