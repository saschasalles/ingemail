<?php
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['role:user']], function () {
    Route::get('/user', 'UserController@index')->name('user');
});

Route::group(['middleware' => ['role:profile']], function (){
    Route::get('/profile', 'ProfileController@index')->name('profile');
});

Route::resource('mails', 'MailController');

Route::get('/', function () {
    return view('welcome');
});

Route::get('reception', function(){
    return view('reception');
});

Route::get('brouillons', function(){
    return view('brouillons');
});

Route::get('envoyes', function(){
    return view('envoyes');
});

Route::get('archives', function(){
    return view('archives');
});

Route::get('indesirables', function(){
    return view('indesirables');
});

Route::get('corbeille', function(){
    return view('corbeille');
});

Auth::routes();

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::get('/home', 'HomeController@index')->name('home');
