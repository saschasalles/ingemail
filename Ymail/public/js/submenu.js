$('.jumbotron').on('contextmenu', function(e) {
  var top = e.pageY - 130;
  var left = e.pageX - 440;
  $("#context-menu").css({
    display: "block",
    top: top,
    left: left
  }).addClass("show");
  return false;
}).on("click", function() {
  $("#context-menu").removeClass("show").hide();
});

$("#context-menu a").on("click", function() {
  $(this).parent().removeClass("show").hide();
});

$("html").on("click", function() {
    $("#context-menu").removeClass("show").hide();
});


$("html").mouseup(function(e) {
    var container = $("#tablist");
    if (!container.is(e.target) && container.has(e.target).length === 0) {
        $('.active').toggleClass('active');
    }
});


$("").click(function(){
  $("#list-send").hide();
});
